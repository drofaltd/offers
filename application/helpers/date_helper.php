<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
function date_countdown($date, $reversed = false, $number_tag_open = '<span class="date_countdown_num">', $number_tag_close = "</span>") {
	$CI = &get_instance();
	$CI->load->helper('strings_helper');
	//$date = "2035-02-14 12:06:23";
	$Date = new DateTime();
	$Interval = $Date->diff(new DateTime($date));

	$date_arr = [
		'years' => (int) $Interval->format('%r%y'),
		'months' => (int) $Interval->format('%r%m'),
		'days' => (int) $Interval->format('%r%d'),
		'hours' => (int) $Interval->format('%r%h'),
		'minutes' => (int) $Interval->format('%r%i'),
			//	'seconds' => (int) $Interval->format('%r%s'),
	];

	foreach ($date_arr as $key => $item) {
		if ($reversed) {
			$item *= -1;
		}
		if ($item < 0)
			return false; //date too old
		if ($item == 0 && $key != 'minutes')
			continue; //no need zero value in output

		switch ($key) {
			case 'years':
				$item_names = ['год', 'года', 'лет'];
				break;
			case 'months':
				$item_names = ['месяц', 'месяца', 'месяцев'];
				break;
			case 'days':
				$item_names = ['день', 'дня', 'дней'];
				break;
			case 'hours':
				$item_names = ['час', 'часа', 'часов'];
				break;
			case 'minutes':
				$item_names = ['мин.', 'мин.', 'мин.'];
				break;
//			case 'seconds':
//				$item_names = ['сек.', 'сек.', 'сек.'];
//				break;
		}

		$date_strings[$key] = $number_tag_open . $item . $number_tag_close . ' ' . numberedWord($item, $item_names);
	}

	if (!empty($date_strings)) {
		return implode(', ', $date_strings);
	}
}

function date_convert($date, $from_format, $to_format) {
	$Date = DateTime::createFromFormat($from_format, $date);
	return $Date->format($to_format);
}

function get_hours_range() {
	$hours = range(0, 24);
	foreach ($hours as &$item) {
		if (strlen($item) == 1) {
			$item = '0' . $item;
		}
	}
	return $hours;
}

function get_minutes_range() {
	$hours = range(0, 59);
	foreach ($hours as &$item) {
		if (strlen($item) == 1) {
			$item = '0' . $item;
		}
	}
	return $hours;
}

function months_range() {
	return [
		1 => 'Январь',
		2 => 'Февраль',
		3 => 'Март',
		4 => 'Апрель',
		5 => 'Май',
		6 => 'Июнь',
		7 => 'Июль',
		8 => 'Август',
		9 => 'Сентябрь',
		10 => 'Октябрь',
		11 => 'Ноябрь',
		12 => 'Декабрь'
	];
}