<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
function remove_decimal_triling_zeros($val){
	if($val != round($val)){
		return $val;
	}
	return round($val);
}