<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */

function numberedWord($num, $words) {
	$num = abs($num);
	if ($num <= 20) {
		if ($num == 1) {
			return $words[0];
		}
		if ($num >= 2 && $num <= 4) {
			return $words[1];
		}
		if ($num == 0 || $num >= 5) {
			return $words[2];
		}
	}
	return numberedWord($num % 10, $words);
}

function processLocationName($name){
		$name = strip_tags(trim(ucwords(strtolower($name))));
		return $name;
}