<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */

function encodeUrlHash($arr){
	$prop_arr;
	foreach($arr as $key=>$val){
		$prop_arr[] = $key.':'.$val;
	}
	return implode(',',$prop_arr);
}