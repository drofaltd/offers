<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
function encrypt_image($path){
	$fh = fopen($path, "r");
	$imgbinary = fread($fh, filesize($path));
	fclose($fh);
	return base64_encode($imgbinary);
}