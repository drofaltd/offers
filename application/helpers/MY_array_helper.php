<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */

function group_db_field($field,$array){
	if(empty($array)){
		return [];
	}
	$ret_array = [];
	foreach ($array as $item){
		if(!isset($item[$field])){
			continue;
		}
		$ret_array[] = $item[$field];
	}
	
	return $ret_array;
}