<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
function deleteFileByName($path, $name, $exclude = []) {
	$dir = scandir($path);
	foreach ($dir as $file) {
		if (!is_file($path.$file))
				continue;
		$fn_arr = explode(".", $file);
		unset($fn_arr[count($fn_arr) - 1]);
		$filename = implode(".", $fn_arr);
		if ($filename == $name && !in_array($file, $exclude) && file_exists($path . "/" . $file)) {
			unlink($path . "/" . $file);
		}
	}
}