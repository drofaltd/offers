<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$_GET = $this->uri->ruri_to_assoc(3);
	}

	public function index() {
		try {
			if (!$this->access->checkAccess(__METHOD__)) {
				redirect('site/login');
			}

			$this->layout->view('site/index');
		} catch (MyException $ex) {
			$this->layout->view('site/index', ['error' => $e->getMessage()]);
		}
	}

	public function login() {
		try {
			$this->load->library('form_validation');
			$this->load->helper('form');

			if ($this->input->post()) {
				if ($this->form_validation->run()) {
					if ($this->access->loginUser($this->input->post('email'), $this->input->post('password'))) {
						redirect(base_url('offers'));
					} else {
						$this->session->set_flashdata('error', 'Неправильный логин или пароль');
						redirect(base_url('site/login'));
					}
				}
			}

			$this->layout->view('site/login');
		} catch (MyException $ex) {
			$this->layout->view('site/login', ['error' => $ex->getMessage()]);
		}
	}

	public function logout() {
		$this->access->logoutUser();
		redirect('');
	}

	public function forgot_password() {
		$this->load->helper('form');
		$data = array();
		if ($this->input->post()) {
			if ($this->user_model->forgotPassword()) {
				$this->session->set_flashdata('notice', 'Ссылка для восстановления пароля отправлена на указанный email');
				redirect('/site/forgot_password');
			} else {
				$data['error'] = $this->user_model->getError();
			}
		}
		$this->layout->view('site/forgot_password', $data);
	}

	public function reset_password() {
		$this->load->helper('form');
		$data = array();

		if ($this->user_model->checkResetPasswordToken()) {
			if ($this->input->post()) {
				$pass_data = ['password' => $this->user_model->encodePassword($this->input->post('password', TRUE))];
				if ($this->user_model->resetPassword($pass_data, $this->input->get('user_id', TRUE))) {
					$this->session->set_flashdata('notice', 'Ваш пароль изменен, теперь вы можете залогиниться на сайте');
					redirect('/site/login');
				} else {
					$data['error'] = $this->user_model->getError();
				}
			}
		} else {
			$data['error'] = 'Неправильные параметры запроса, попробуйте запросить ссылку для восстановления еще раз или обратитесь к администраторуы';
		}
		$this->layout->view('site/reset_password', $data);
	}

	public function notice() {
		$this->layout->view('site/notice');
	}

	public function error() {
		$this->layout->view('site/error');
	}

	public function show404() {
		show_404();
	}

	public function show403() {
		show_error($this->session->flashdata('error403'), 403);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */