<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class BaseAjax extends CI_Controller {

	public function __construct() {
		parent::__construct();
		header('Content-type: application/json');
	}
	
	protected function jsonAnswer($answer = []) {
		$answer = json_encode($answer);
		$this->logAnswer($answer);
		echo $answer;
		exit;
	}
	
	
	private function logAnswer($answer) {
	
		if (!$this->config->item('log_ajax_answers')) {
			return;
		}
		$log = date(DATE_D_STANDART) . ': ' . $_SERVER['REMOTE_ADDR'] . ':' . $_SERVER['REMOTE_PORT'] . ' ' . current_url() . "\n post:";
		ob_start();
		print_r($_POST);
		$log.= ob_get_clean();
		$log.="\nanswer:" . $answer . "\n\n";
		$log_path = APPPATH . 'logs/ajax';
		if (!is_dir($log_path)) {
			mkdir($log_path,0777);
		}
		file_put_contents($log_path . '/ajax-' . date('Y-m-d H') . '.log', $log, FILE_APPEND);
	}

}