<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');


include 'baseAjax.php';

class OffersTableEditorAjax extends BaseAjax {

	public function __construct() {
		parent::__construct();
		$_GET = $this->uri->ruri_to_assoc(3);
	}

	public function saveField() {
		try {
			$this->load->library('form_validation');
			$this->load->model('edition_field_model');

			if (!$this->access->checkAccess(__METHOD__)) {
				$this->jsonAnswer(['state' => 'redirect', 'url' => 'site/login']);
			}

			$field_id = $this->input->post('field_id');


			$this->config->load("form_validation");
			$vconf = $this->config->item('offersTableEditorAjax/saveField');
			if ($field_id) {
				$vconf[] = [
					'field' => 'name',
					'label' => 'Заголовок',
					'rules' => 'required|is_unique_except_id[edition_field.name;' . $field_id . ']'
				];
			} else {
				$vconf[] = [
					'field' => 'name',
					'label' => 'Заголовок',
					'rules' => 'required|is_unique[edition_field.name]'
				];
			}
			$this->form_validation->set_rules($vconf);

			if (!$this->form_validation->run()) {
				$this->jsonAnswer(['state' => 'error', 'msg' => $this->form_validation->error_msg()]);
			}

			$field_id = $this->edition_field_model->saveField($this->prepareSaveFieldData(), $field_id);
			$field_info = $this->edition_field_model->get(['fields' => $this->edition_field_model->field_item_fields, 'search' => ['id' => $field_id]]);
			$this->jsonAnswer(['state' => 'ok', 'field_info' => $field_info]);
		} catch (MyException $exc) {
			$this->jsonAnswer(['state' => 'error', 'msg' => $exc->getMessage()]);
		}
	}

	private function prepareSaveFieldData() {
		$data['field_data']['name'] = $this->input->post('name');
		$data['field_data']['type'] = $this->input->post('type');
		$data['field_data']['bg_color'] = $this->input->post('bg_color');
		$data['field_data']['color'] = $this->input->post('color');
		$data['field_view_role_ids'] = $this->input->post('can_view_role_ids');
		$data['field_edit_role_ids'] = $this->input->post('can_edit_role_ids');

		$data['field_data']['type_options'] = [];
		if(in_array($data['field_data']['type'],$this->edition_field_model->list_inputs)) {
			$field_type_option_names = $this->input->post('field_type_option_name');
			$field_type_option_values = $this->input->post('field_type_option_value');

			$data['field_data']['type_options']['list'] = [];
			foreach ($field_type_option_names as $key => $field_type_option_name) {
				if (!isset($field_type_option_values[$key])) {
					throw new MyException('Количество имен опций не совпадает с количеством значений');
				}
				$data['field_data']['type_options']['list'][] = ['name' => $field_type_option_name, 'value' => $field_type_option_values[$key]];
			}
			
		}
		$data['field_data']['type_options'] = json_encode($data['field_data']['type_options']);
		
		return $data;
	}

	public function saveSorters() {
		try {
			$this->load->library('form_validation');
			$this->load->model('edition_field_model');
			if (!$this->access->checkAccess(__METHOD__)) {
				$this->jsonAnswer(['state' => 'redirect', 'url' => 'site/login']);
			}

			$sorters = $this->input->post('sorters');

			if ($this->form_validation->run()) {
				$this->edition_field_model->saveSorters($sorters);
			} else {
				$this->jsonAnswer(['state' => 'error', 'msg' => $this->form_validation->error_msg()]);
			}

			$this->jsonAnswer(['state' => 'ok']);
		} catch (MyException $exc) {
			$this->jsonAnswer(['state' => 'error', 'msg' => $exc->getMessage()]);
		}
	}

	public function getFormFieldInfo() {
		try {
			$this->load->model('edition_field_model');
			if (!$this->access->checkAccess(__METHOD__)) {
				$this->jsonAnswer(['state' => 'redirect', 'url' => 'site/login']);
			}

			$field_id = $this->input->post('field_id');

			$form_fields = $this->edition_field_model->getFieldFormFields($field_id);

			$form_fields['type_options'] = json_decode($form_fields['type_options']);

			$this->jsonAnswer(['state' => 'ok', 'form_fields' => $form_fields]);
		} catch (MyException $exc) {
			$this->jsonAnswer(['state' => 'error', 'msg' => $exc->getMessage()]);
		}
	}

}
