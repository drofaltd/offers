<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Offers extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$_GET = $this->uri->ruri_to_assoc(3);
	}

	public function index() {
		try {
			$this->load->model('offer_model');
			$this->load->model('edition_field_model');

			if (!$this->access->checkAccess(__METHOD__)) {
				redirect('site/login');
			}
			$count = $this->input->get('count');
			$from = 0;
			if (empty($count)) {
				$count = 5000;
			}

			$data['ed_fields'] = $this->edition_field_model->getAll(['fields'=>['name','type'],'order_by' => ['sorter' => 'ASC']]);
			$data['offers'] = $this->offer_model->getAll([
				'fields' => ['username', 'edition_ipn', 'edition_rin', 'edition_fields'],
				'search' => ['last_edition' => 1],
				'limit' => ['from' => 0, 'count' => $count],
				'param' => ['ed_fields' => $data['ed_fields']],
			]);
			
			$this->layout->view('offers/index', $data);
		} catch (MyException $ex) {
			$this->layout->view('offers/index', ['error' => $ex->getMessage()]);
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */