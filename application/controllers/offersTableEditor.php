<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class OffersTableEditor extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$_GET = $this->uri->ruri_to_assoc(3);
	}

	public function index() {
		try {
			$this->load->model('role_model');
			$this->load->model('edition_field_model');
			if (!$this->access->checkAccess(__METHOD__)) {
				redirect('site/login');
			}
			$data['roles'] = $this->role_model->getAll(['fields'=>['*']]);
			
			$data['json_fields'] = json_encode($this->edition_field_model->getAll(['fields'=>$this->edition_field_model->field_item_fields,'order'=>['sorter'=>'ASC']]));
			
			$data['list_inputs'] = json_encode($this->edition_field_model->list_inputs);
			$this->layout->view('offersTableEditor/index', $data);
		} catch (MyException $exc) {
			$this->layout->view('offersTableEditor/index', ['error' => $exc->getMessage()]);
		}
	}

}
