<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Service extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$_GET = $this->uri->ruri_to_assoc(3);
	}

	public function testFlush() {
		ob_implicit_flush(1);
		for ($i = 0; $i < 2; $i++) {
			$this->fecho($i);
			sleep(1);
		}
	}

	public function fecho($string) {

		ob_end_flush();
//		ob_end_clean();
		echo $string . "<br>";
		ob_start();

//		ob_flush();
	}

	public function genOffers() {
		return;
		try {
			$this->load->model('offer_model');

			if (!$this->access->checkAccess(__METHOD__)) {
				redirect('site/login');
			}

			$this->load->model('offer_model');

			for ($i = 0; $i < 30000; $i++) {
				$offer = [
					'user_id' => 1,
					'ipn' => random_string(),
					'field1' => random_string(),
					'field2' => random_string(),
					'field3' => random_string(),
					'field4' => random_string(),
					'field5' => random_string(),
					'field6' => random_string(),
					'field7' => random_string(),
				];

				$this->offer_model->save($offer);
				if ($i % 1000 == 0) {
					$this->fecho('Вставлено ' . $i . ' предложек');
				}
			}
			print 'ok';
		} catch (MyException $ex) {
			print $e->getMessage();
		}
	}

}
