<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');


include 'baseAjax.php';

class OffersAjax extends BaseAjax {

	public function __construct() {
		parent::__construct();
		$_GET = $this->uri->ruri_to_assoc(3);

		$this->load->model('offer_model');
	}

}
