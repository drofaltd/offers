<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Access {

	private $CI;

	public function __construct() {
		$this->CI = & get_instance();
	}

	/**
	 * 
	 * @param string $access_key
	 * @return boolean
	 */
	public function checkAccess($access_key) {
		$this->CI->load->config('access', true);
		$access_config = $this->CI->config->item('access');
		$role = $this->getSessionUserRoleName();
		if (!isset($access_config[$role])) {
			return false;
		}
		$privs = $access_config[$role];

		if ((in_array($access_key, $privs['allow']) || in_array('*', $privs['allow'])) && (!isset($privs['disallow']) || !in_array($access_key, $privs['disallow']))) {
			return true;
		}

		return false;
	}

	public function checkAccessOld($roles_names = '') {
		$curr_user = $this->getSessionUser();
		if (empty($curr_user['userRoleId']) || !$curr_user['userRoleId']) {
			return false;
		}
		$roles = $this->getRoles();
		if (is_string($roles_names)) {
			return $roles_names == $roles[$roles_names];
		}
		if (is_array($roles_names)) {
			foreach ($roles_names as $role) {
				if ($roles[$role] == $curr_user['userRoleId']) {
					return true;
				}
			}
		}

		return false;
	}

	public function encodeCookieArr() {

		$all_user_data_serilized = serialize($this->CI->session->all_userdata());
		$_cookie_value = $all_user_data_serilized . md5($all_user_data_serilized . $this->CI->config->item('encryption_key'));

		return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->CI->config->item('encryption_key'), $_cookie_value, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
	}

	public function decodeCookieArr($str) {
		return unserialize(base64_decode($str));
	}

	public function getSessionUser() {
		return $this->CI->session->userdata('user');
	}

	public function getSessionUserId() {
		$user = $this->CI->session->userdata('user');
		if (empty($user['user_id'])) {
			return null;
		}
		return $user['user_id'];
	}

	public function getSessionUserRoleName() {
		$user = $this->getSessionUser();
		if (!isset($user['role_name'])) {
			return null;
		}
		return $user['role_name'];
	}

	/**
	 * 
	 * @param string $email
	 * @param string $password
	 * @return array
	 */
	public function loginUser($email, $password) {
		return $this->loginUserBy(['email' => $email, 'password' => $this->CI->user_model->encodePassword($password)]);
	}

	/**
	 * 
	 * @param array $search
	 * @return boolean
	 * @throws AccessException
	 */
	public function loginUserBy($search) {
		$user = $this->CI->user_model->get(['fields' => ['email', 'login', 'role_name'], 'search' => $search]);
		if ($user) {
			$this->CI->session->set_userdata('user', $user);
			return $user;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param string $email
	 * @param string $password
	 * @return boolean
	 */
	public function rememberUser($email, $password) {
		$expire = 30 * 24 * 3600; //month
		set_cookie($this->CI->config->item('cookie_remember_me_email_name'), $this->CI->encrypt->encode($email), $expire);
		set_cookie($this->CI->config->item('cookie_remember_me_password_name'), $this->CI->encrypt->encode($password), $expire);
		return true;
	}

	public function forgotUser() {
		delete_cookie($this->CI->config->item('cookie_remember_me_email_name'));
		delete_cookie($this->CI->config->item('cookie_remember_me_password_name'));
		return true;
	}

	public function updateUserSession($params) {
		$user = $this->CI->session->userdata('user');
		foreach ($params as $key => $item) {
			$user[$key] = $item;
		}
		$this->CI->session->set_userdata('user', $user);
	}

	public function logoutUser() {
		$this->CI->session->sess_destroy();
		$this->forgotUser();
	}

	public function isLoggedIn() {
		return $this->CI->session->userdata('user');
	}

	private function getRoles() {
		return $this->roles;
	}

}

class AccessException extends MyException {
	
}
