<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class MY_Form_validation extends CI_Form_validation {

	function __construct($config = array()) {
		parent::__construct($config);
	}

	/**
	 * Error Array
	 *
	 * Returns the error messages as an array
	 *
	 * @return  array
	 */
	function error_array() {
		if (count($this->_error_array) === 0) {
			return FALSE;
		}
		else
			return $this->_error_array;
	}

	function error_msg($delimiter = '<br>') {
		if (count($this->error_array()) === 0) {
			return FALSE;
		} else {
			return implode($delimiter, $this->error_array());
		}
	}

	/**
	 * Get the value from a form
	 *
	 * Permits you to repopulate a form field with the value it was submitted
	 * with, or, if that value doesn't exist, with the default
	 *
	 * @access	public
	 * @param	string	the field name
	 * @param	string
	 * @return	void
	 */
	public function set_value($field = '', $default = '') {

		if (!isset($this->_field_data[$field]) OR $this->_field_data[$field]['postdata'] === NULL) {
			return $default;
		}

		// If the data is an array output them one at a time.
		//     E.g: form_input('name[]', set_value('name[]');
		if (is_array($this->_field_data[$field]['postdata'])) {
			return array_shift($this->_field_data[$field]['postdata']);
		}

		return $this->_field_data[$field]['postdata'];
	}

	/**
	 * ENUM
	 * The submitted string must match one of the values given
	 *
	 * usage:
	 * enum[value_1, value_2, value_n]
	 *
	 * example (any value beside exactly 'ASC' or 'DESC' are invalid):
	 * $rule['order_by'] = "required|enum[ASC,DESC]";
	 * 
	 * example of case-insenstive enum using strtolower as validation rule
	 * $rule['favorite_corey'] = "required|strtolower|enum[feldman]";
	 *
	 * @access    public
	 * @param     string $str the input to validate
	 * @param     string $val a comma separated lists of values
	 * @return    bool
	 */
	function enum($str, $val = '') {

		if (empty($val)) {
			return FALSE;
		}

		$arr = explode(',', $val);
		$array = array();
		foreach ($arr as $value) {
			$array[] = trim($value);
		}

		return (in_array(trim($str), $array)) ? TRUE : FALSE;
	}

	// --------------------------------------------------------------------

	/**
	 * NOT ENUM
	 * The submitted string must NOT match one of the values given
	 *
	 * usage:
	 * enum[value_1, value_2, value_n]
	 *
	 * example (any input beside exactly 'feldman' or 'haim' are valid):
	 * $rule['favorite_corey'] = "required|not_enum[feldman,haim]";
	 *
	 * @access   public
	 * @param    string $str the input to validate
	 * @param    string $val a comma separated lists of values
	 * @return   bool
	 */
	function not_enum($str, $val = '') {
		$str = strtolower($str);
		return ($this->enum($str, $val) === TRUE) ? FALSE : TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Alpha-numeric-blank
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	public function alpha_numeric_blank($str) {
		return (!preg_match("/^([a-z0-9\s])+$/i", $str)) ? FALSE : TRUE;
	}

	public function restricted_email_domains($str, $val = '') {
		if (!$val) {
			return true;
		}
		$restr_domains = explode(',', $val);
		$email_arr = explode('@', $str);
		$domain = $email_arr[1];
		if (in_array($domain, $restr_domains)) {
			return false;
		}
		return true;
	}

	public function is_unique_except_id($str, $params) {
		list($field, $id) = explode(';', $params);

		list($table, $field) = explode('.', $field);
		$query = $this->CI->db
				->limit(1)
				->where($field, $str)
				->where('id!=', $id, FALSE)
				->get($table);

		return $query->num_rows() === 0;
	}
	
	public function decimal_positive($str)
	{
		if((bool) preg_match('/^[0-9]+$/', $str)){
			return true;
		}
		
		return (bool) preg_match('/^[0-9]+\.[0-9]+$/', $str);
	}

	/////custom project validations
	///////////////////////////////////////////////////////////////////////////////

	
}