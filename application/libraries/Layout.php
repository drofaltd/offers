<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Layout {

	var $CI;
	var $layout;
	var $left_sidebar;
	var $right_sidebar;

	function __construct($layout = "layout/main") {
		$this->CI = & get_instance();
		$this->layout = $layout;
	}

	function setLayout($layout) {
		$this->layout = $layout;
	}
	function getLayout() {
		return $this->layout;
	}

	function view($view, $data = null, $return = false,$params=[]) {
		$loadedData['header_bar'] = $this->getHeaderBar($params);
		$loadedData['user_auth_sidebar'] = $this->getUserAuthSidebar();
		$loadedData['content_for_layout'] = $this->CI->load->view($view, $data, true);
		$loadedData['left_sidebar'] = $this->getLeftSidebar();
		if ($return) {
			$output = $this->CI->load->view($this->layout, $loadedData, true);
			return $output;
		} else {
			$this->CI->load->view($this->layout, $loadedData, false);
		}
	}
	
	function partialView($view, $data = null, $return = false) {
		return $this->CI->load->view($view, $data, $return);
	}

	private function getHeaderBar($params) {
		$this->CI->load->model('widgets/header_bar_widget');
		return $this->CI->header_bar_widget->get($params);
	}
	
	private function getUserAuthSidebar() {
		$this->CI->load->model('widgets/user_auth_sidebar_widget');
		return $this->CI->user_auth_sidebar_widget->get();
	}
	
	private function getLeftSidebar() {
		$this->CI->load->model('widgets/base_pagesbased_widget');
		$this->CI->load->model('widgets/left_sidebar_widget');
		$left_sidebar_content = $this->CI->left_sidebar_widget->get();
		if(!$left_sidebar_content){
			return '';
		}
		return $left_sidebar_content;
	}
}