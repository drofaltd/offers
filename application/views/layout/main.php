<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width">
		<script>
			var BASE_URL = '<?= base_url() ?>';
		</script>
		<script type="text/javascript" src="<?= base_url('js/common.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('js/jquery/jquery-1.11.0.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('js/jquery/jquery-ui-1.10.3.custom.min.js') ?>"></script>
		<?= $header_bar ?>
		<link rel="stylesheet" href="<?= base_url('css/reset.css') ?>">
		<link rel="stylesheet" href="<?= base_url('css/jquery/ui-lightness/jquery-ui-1.10.3.custom.css') ?>">
		<link rel="stylesheet" href="<?= base_url('css/style.css') ?>">
	</head>
	<body>
		<div class="wrapper">
			<header class="header">
				<div>
					<div class="user-auth-wr">
<?= $user_auth_sidebar ?>
					</div>
					<div class="layout-title"></div>
				</div>		
			</header><!-- /header-->
			<section class="middle">
				<div class="container">
<?= $content_for_layout ?>
				</div><!-- /container -->
<?= $left_sidebar ?>
			</section>
		</div><!-- /wrapper -->
		<div id="modal-box" class="hide"></div>
		<div class="ajax-loading-wr"></div>
	</body>
</html>