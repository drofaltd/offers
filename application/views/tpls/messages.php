<?php if ($this->session->flashdata('notice')) { ?>
	<div class="notice">
		<i class="icon-close"></i>
		<?= $this->session->flashdata('notice') ?>
	</div><!-- /notice -->
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
	<div class="err-notice">
		<i class="icon-close"></i>
		<?= $this->session->flashdata('error') ?>
	</div><!-- /notice -->
<?php } ?>
<?php if (!empty($error)) { ?>
	<div class="error-wr">
		<i class="icon-close"></i>
		<?= $error ?>
	</div><!-- /notice -->
<?php } ?>
<?php if (!empty($notice)) { ?>
	<div class="notice">
		<i class="icon-close"></i>
		<?= $notice ?>
	</div><!-- /notice -->
<?php } ?>