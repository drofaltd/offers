<div class="content site-login-page">
	<h1>Вход</h1>
	<?= $this->layout->partialView('tpls/messages') ?>
	<?php if ($this->form_validation->error_array()) { ?>
		<div class="err-notice">Некоторые поля заполнены не правильно</div>
	<?php } ?>
	<form method="post">
		<fieldset>
			<div>
				<label>
					Email<span class="req">*</span>:<br> <?= form_error('email') ?> <input type="email" name="email" value="" required/>
				</label>
			</div>
			<div>
				<label>
					Пароль<span class="req">*</span>:<br> <?= form_error('password') ?> <input type="password" name="password" required/>
				</label>
			</div>
			<div>
				<input type="submit" name="submit" value="Войти" />
			</div>
		</fieldset>
	</form>
</div>