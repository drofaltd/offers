<title><?php if (!empty($title)) { ?><?= $title ?><?php } else { ?>Projectshub<?php } ?></title>
<?php
if (!empty($header_js_scripts)) {
	foreach ($header_js_scripts as $header_js_script_src) {
		?><script type="text/javascript" src="/js/<?= $header_js_script_src ?>.js"></script>
		<?php
	}
}
if (!empty($header_css)) {
	foreach ($header_css as $css) {
		?><link rel="stylesheet" type="text/css" href="/css/<?= $css ?>.css">
		<?php
	}
}
?>