<div class="content">
	<h1>Предложки</h1>
	<?= $this->layout->partialView('tpls/messages') ?>
	<div>
		<div class="offers-table-wr">
			<table class="offers-table">
				<tr>
					<th class="first-column"><div class="resizable">&nbsp;</div></th>
					<th><div class="resizable">id</div></th>
					<th><div class="resizable">ИПН</div></th>
					<th><div class="resizable">РИН</div></th>
					<th><div class="resizable">Пользователь</div></th>
					<?php foreach ($ed_fields as $ed_field) { ?>
						<th class="resizable"><div class="resizable"><?= $ed_field['name'] ?></div></th>
					<?php } ?>
					<th></th>
				</tr>
				<?php foreach ($offers as $offer) { ?>
					<tr data-id="<?= $offer['id'] ?>" class="offer-item">
						<td class="first-column"><div class="resizable">&nbsp;</div></td>
						<td><div class="resizable"><?= $offer['id'] ?></div></td>
						<td><div class="resizable"><?= $offer['edition_ipn'] ?></div></td>
						<td><div class="resizable"><?= $offer['edition_rin'] ?></div></td>					
						<td><div class="resizable"><?= $offer['username'] ?></div></td>
						<?php foreach ($ed_fields as $ed_field) { ?>
							<td><div class="resizable"><?= $offer[$this->edition_field_model->edition_field_prefix . $ed_field['id']] ?></div></td>
						<?php } ?>
							<td><div class="resizable"><a href="#" class="edit-offer-button">Изменить</a></div></td>
					</tr>
				<?php } ?>
			</table>
		</div>
	</div>
</div>