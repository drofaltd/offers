<div class="content">
	<a href="#" class="add-field-button">Добавить поле</a>
	<div>
		<ul class="fields-edit-list sortable"></ul>
	</div>
</div>

<script>
	var fileds = <?= $json_fields ?>;
	var listInputs = <?= $list_inputs ?>;
	var formFields;
</script>

<div class="hide">
	<div class="field-item-tpl">
		<li data-id="" title="123&#13;sdf" class="field-item">
			<div class="name-wr edit-field-button"></div>
		</li>
	</div>


	<div class="edit-field-form-tpl">
		<div class="edit-field-form-wr">
			<div></div>
			<form class="edit-field-form">
				<input type="hidden" name="field_id" value="0" />
				<input type="hidden" name="bg_color" value="" />
				<input type="hidden" name="color" value="" />
				<div>
					<div class="field-header">Заголовок:</div>
					<div class="field"><input type="text" name="name" value="" required/></div>
				</div>
				<div>
					<div class="field-header">Тип поля:</div>
					<div class="field">
						<select name="type">
							<option value="text">Текст</option>
							<option value="number">Число</option>
							<option value="date">Дата</option>
							<option value="hyperlink">Гиперссылка</option>
							<option value="select">Селект</option>
							<option value="radio">Радио</option>
							<option value="checkbox">Чекбокс</option>
						</select>
					</div>
				</div>
				<div>
					<div class="field-header"></div>
					<div class="field"><div class="field-type-options-wr"></div></div>
				</div>
				<div>
					<div class="field-header">Цвет фона:</div>
					<div class="field"><div class="bgColorSelector"><div></div></div></div>
				</div>
				<div>
					<div class="field-header">Цвет текста:</div>
					<div class="field"><div class="colorSelector"><div></div></div></div>
				</div>
				<div>
					<div class="field-header">Видно для:</div>
					<div class="field">
						<?php foreach ($roles as $role) { ?>
							<label><input type="checkbox" name="can_view_role_ids[]" value="<?= $role['id'] ?>" checked />&nbsp;&nbsp;<?= $role['name'] ?></label>
						<?php } ?>
					</div>
				</div>
				<div>
					<div class="field-header">Разрешено редактировать:</div>
					<div class="field">
						<?php foreach ($roles as $role) { ?>
							<label><input type="checkbox" name="can_edit_role_ids[]" value="<?= $role['id'] ?>" checked/>&nbsp;&nbsp;<?= $role['name'] ?></label>
						<?php } ?>
					</div>
				</div>
				<div>
					<div class="field-header"></div>
					<div class="field">
						<input type="submit" value="Сохранить"/>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="list-options-wr-tpl">
		<div class="list-options-wr">
			<div>
				<a href="#" class="add-field-type-list-option-button">Добавить опцию</a>
			</div>
			<ul class="field-type-list-options field-type-list-options-sortable"></ul>
		</div>
	</div>

	<div class="field-type-list-option-item-tpl">
		<li class="field-type-list-option-item">
			<span>
				<input type="text" name="field_type_option_name[]" title="Заголовок опции" placeholder="Заголовок опции"/>
				<input type="text" name="field_type_option_value[]" title="Значение опции" placeholder="Значение опции"/>
			</span>
			<span class="delete-field-type-list-option-button">-</span>
		</li>
	</div>
</div>