<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Edition_field_model extends Simple_db_model {

	public $field_item_fields = ['*'],
			$list_inputs = ['select', 'radio', 'checkbox'],
			$edition_field_prefix = 'editionfield_';

	public function __construct() {
		parent::__construct();
		parent::setTable('edition_field');
	}

	public function setFields($fields) {
		foreach ($fields as $key => $field) {
			switch ($field) {
				case 'can_view_perms':
					$this->need_additional_loop = true;
					break;
				case 'can_edit_perms':
					$this->need_additional_loop = true;
					break;
				default : $this->select_arr[$field] = $this->table . '.' . $field;
			}
		}
	}

//	public function getAll($params = []) {
//
//		$fields = isset($params['fields']) ? $params['fields'] : [];
//		$search = isset($params['search']) ? $params['search'] : [];
//		$from = isset($params['limit']['from']) ? $params['limit']['from'] : 0;
//		$count = isset($params['limit']['count']) ? $params['limit']['count'] : 0;
//		$order = isset($params['order']) ? $params['order'] : [];
//
//		$filtered_select = false;
//		$additional_loop = false;
//
//		if ($from || $count) {
//			$this->db->limit($count, $from);
//		}
//
//		$select_arr = [$this->table . '.id'];
//
//		$join_arr = $group_by_arr = [];
//		foreach ($fields as $key => $field) {
//			switch ($field) {
//				case 'can_view_perms':
//					$additional_loop = true;
//					break;
//				case 'can_edit_perms':
//					$additional_loop = true;
//					break;
//				default : $select_arr[$field] = $this->table . '.' . $field;
//			}
//		}
//
//
//		$select_str = implode(',', $select_arr);
//		$this->db->select($select_str, $filtered_select);
//		$this->db->from($this->table);
//
//		foreach ($search as $key => $val) {
//			switch ($key) {
//				case '_field_like':
//					$this->db->where($this->table . '.' . $val['field'] . ' LIKE "%' . $val['value'] . '%"', NULL, false);
//					break;
//				case '_in':
//					$this->db->where_in($this->table . '.' . $val['field'], $val['value']);
//					break;
//				default :
//					$this->db->where($this->table . '.' . $key, $val);
//					break;
//			}
//		}
//
//		if (!empty($join_arr)) {
//			foreach ($join_arr as $val) {
//				$this->db->join($val['table'], $val['on'], isset($val['param']) ? $val['param'] : '');
//			}
//		}
//
//		if (!empty($group_by_arr)) {
//			foreach ($group_by_arr as $val) {
//				$this->db->group_by($val);
//			}
//		}
//
//		if (!empty($order)) {
//			foreach ($order as $key => $val) {
//				switch ($key) {
//					default :$this->db->order_by($this->table . '.' . $key, $val);
//				}
//			}
//		}
//
//		$items = $this->db->get()->result('array');
//
//		if ($additional_loop) {
//			foreach ($items as &$item) {
//				if (in_array('can_view_perms', $fields)) {
//					$this->load->model('edition_field_perm_model');
//					$item['can_view_perms'] = $this->edition_field_perm_model->getRoleIdsByType($this->edition_field_perm_model->can_view_perm_type, $item['id']);
//				}
//				if (in_array('can_edit_perms', $fields)) {
//					$this->load->model('edition_field_perm_model');
//					$item['can_edit_perms'] = $this->edition_field_perm_model->getRoleIdsByType($this->edition_field_perm_model->can_edit_perm_type, $item['id']);
//				}
//			}
//		}
//
//		return $items;
//	}

	protected function additionalLoop() {
		if (!$this->need_additional_loop) {
			return;
		}
		$fields = !empty($this->params['fields']) ? $this->params['fields'] : [];

		foreach ($this->items as &$item) {
			if (in_array('can_view_perms', $fields)) {
				$this->load->model('edition_field_perm_model');
				$item['can_view_perms'] = $this->edition_field_perm_model->getRoleIdsByType($this->edition_field_perm_model->can_view_perm_type, $item['id']);
			}
			if (in_array('can_edit_perms', $fields)) {
				$this->load->model('edition_field_perm_model');
				$item['can_edit_perms'] = $this->edition_field_perm_model->getRoleIdsByType($this->edition_field_perm_model->can_edit_perm_type, $item['id']);
			}
		}
	}

	public function saveField($data, $item_id = 0) {
		$this->load->model('edition_field_perm_model');
		$this->db->trans_begin();

		$item_id = $this->save($data['field_data'], $item_id);

		$this->edition_field_perm_model->saveFieldPerms($this->edition_field_perm_model->can_view_perm_type, $data['field_view_role_ids'], $item_id);
		$this->edition_field_perm_model->saveFieldPerms($this->edition_field_perm_model->can_edit_perm_type, $data['field_edit_role_ids'], $item_id);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			throw new EditionFieldModelException('Ошибка транзакции базы данных');
		} else {
			$this->db->trans_commit();
			return $item_id;
		}
	}

	public function saveSorters($sorters) {
		foreach ($sorters as $field_id => $sorter) {
			$this->save(['sorter' => $sorter], $field_id);
		}
	}

	public function getFieldFormFields($field_id) {
		if (!$field_id) {
			throw new EditionFieldModelException('параметр "field_id" отсутствует');
		}

		$fields = $this->get(['fields' => ['*', 'can_view_perms', 'can_edit_perms'], 'search' => ['id' => $field_id]]);
		return $fields;
	}

}

class EditionFieldModelException extends SimpleDbModelException {
	
}
