<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Role_model extends Simple_db_model {

	public function __construct() {
		parent::__construct();
		parent::setTable('role');
	}

}

class RoleModelException extends SimpleDbModelException {
	
}
