<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Account_menu_widget extends CI_Model {

	public function get() {
		$user_id = $this->access->getSessionUserId();
		return $this->layout->partialView('widgets/account_menu', null, true);
	}

}