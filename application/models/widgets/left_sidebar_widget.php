<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Left_sidebar_widget extends Base_pagesbased_widget {

	static $handlers = [
		'site' => 'account_menu_widget',
	];
	
	public function __construct() {
		parent::__construct();
		$this->setHandlers(self::$handlers);
	}

}