<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Base_pagesbased_widget extends CI_Model {
	private $handlers;
	
	public function get() {
		$admin_prefix = $this->uri->segment(1)=='admin'?'admin/':'';
		$segment1 = $admin_prefix.$this->uri->rsegment(1);
		$segment2 = $segment1 . '/' . $this->uri->rsegment(2);
		$segment3 = $segment2 . '/' . $this->uri->rsegment(3);

		$handler = '';
		if (in_array($segment3, array_keys($this->handlers))) {
			$handler = $this->handlers[$segment3];
		} elseif (in_array($segment2, array_keys($this->handlers))) {
			$handler = $this->handlers[$segment2];
		} elseif (in_array($segment1, array_keys($this->handlers))) {
			$handler = $this->handlers[$segment1];
		}
		if ($handler) {
			$this->load->model('widgets/'.$handler);
			return $this->$handler->get();
		}
		return '';
	}
	
	protected function setHandlers($val){
		$this->handlers = $val;
	}
}