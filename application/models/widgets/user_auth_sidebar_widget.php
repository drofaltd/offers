<?php
/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class User_auth_sidebar_widget extends CI_Model{
	public function get(){
		$user = $this->access->getSessionUser();
		if(!$user){
			return $this->layout->partialView('widgets/user_auth_guest',[],true);
		}
		$data = ['user'=>$user];
		return $this->layout->partialView('widgets/user_auth',$data,true);
	}
}