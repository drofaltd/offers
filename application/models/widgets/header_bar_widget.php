<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class header_bar_widget extends CI_Model {

	static $pages = [
		'offersTableEditor/index' => [
			'js' => ['colorpicker/colorpicker','offersTableEditor/index'],
			'css' => ['colorpicker/colorpicker'],
			],
		'offers/index' => [
			'js' => ['offers/index'],
			],
	];

	public function get($params) {
		$admin_prefix = $this->uri->segment(1) == 'admin' ? 'admin/' : '';

		$segment1 = $admin_prefix . $this->uri->rsegment(1);
		$segment2 = $segment1 . '/' . $this->uri->rsegment(2);
		$segment3 = $segment2 . '/' . $this->uri->rsegment(3);

		$opts = [];
		if (in_array($segment3, array_keys(self::$pages))) {
			$opts = self::$pages[$segment3];
		} elseif (in_array($segment2, array_keys(self::$pages))) {
			$opts = self::$pages[$segment2];
		} elseif (in_array($segment1, array_keys(self::$pages))) {
			$opts = self::$pages[$segment1];
		}
		$content = '';

		if(!empty($params['force_header_title'])){
			$opts['title'] = $params['force_header_title'];
		}
		
		if (!empty($opts)) {
			$content = $this->processOpts($opts);
		}
		return $content;
	}

	private function processOpts($opts) {
		foreach ($opts as $key => $item) {
			switch ($key) {
				case 'js':
					$data['header_js_scripts'] = $item;
					break;
				case 'css':
					$data['header_css'] = $item;
					break;
				case 'title':
					if (!empty($item['hadler'])) {
						$this->load->model($item['hadler']);
						$this->$item['hadler']->getTitle();
					} elseif (!empty($item)) {
						$data['title'] = $item;
					}
					break;
			}
		}
		return $this->layout->partialView('widgets/header_bar', $data, true);
	}

}