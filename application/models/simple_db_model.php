<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Simple_db_model extends CI_Model {

	protected $table,
			$params = [],
			$select_arr = [],
			$join_arr = [],
			$where_arr = [],
			$where_in_arr = [],
			$items = false,
			$need_additional_loop = false;

	public function __construct() {
		parent::__construct();
	}

	private function reset() {
		$this->params = [];
		$this->select_arr = [];
		$this->join_arr = [];
		$this->where_arr = [];
		$this->where_in_arr = [];
		$this->items = false;
		$this->need_additional_loop = false;
	}

	public function getAll($params = []) {
		$this->params = $params;
		$fields = isset($params['fields']) ? $params['fields'] : [];
		$search = isset($params['search']) ? $params['search'] : [];
		$from = isset($params['limit']['from']) ? $params['limit']['from'] : 0;
		$count = isset($params['limit']['count']) ? $params['limit']['count'] : 0;
		$order = isset($params['order']) ? $params['order'] : [];

		$filtered_select = false;

		$this->select_arr = [$this->table . '.id'];

		$join_arr = $group_by_arr = $where_arr = $where_in_arr = [];

		$this->setFields($fields);
		$this->setSearch($search);

		if ($from || $count) {
			$this->db->limit($count, $from);
		}

		$this->db->select(implode(',', $this->select_arr), $filtered_select);
		$this->db->from($this->table);
		$this->setJoin($this->join_arr);
		$this->setWhere($this->where_arr);
		$this->setWhere_in($this->where_in_arr);

		if (!empty($group_by_arr)) {
			foreach ($group_by_arr as $val) {
				$this->db->group_by($val);
			}
		}

		if (!empty($order)) {
			foreach ($order as $key => $val) {
				switch ($key) {
					default :$this->db->order_by($this->table . '.' . $key, $val);
				}
			}
		}

		$this->items = $this->db->get()->result('array');
		if (isset($params['cb']['after_get'])) {
			$params['cb']['after_get']();
		}

		$this->additionalLoop();
		$items = $this->items;
		$this->reset();
		return $items;
	}

	public function getCount($search = []) {

		$join_arr = [];

		$this->setSearch($search);

		$this->db->from($this->table);
		$this->setJoin($join_arr);
		$this->setWhere($where_arr);
		$this->setWhere_in($where_in_arr);

		$this->reset();
		return $this->db->count_all_results();
	}

	public function save($data, $item_id = 0, $sanitize = TRUE) {
		$this->db->set($data, $sanitize);
		if ($item_id) {
			if (is_array($item_id)) {
				$this->db->where_in($this->table . '.' . $item_id['field'], $item_id['value']);
			} else {
				$this->db->where('id', $item_id);
			}
			$this->db->update($this->table);
		} else {
			$this->db->insert($this->table);
			$item_id = $this->db->insert_ID();
		}

		if (!$item_id) {
			throw new SimpleDbModelException('Ошибка записи в базу');
		}

		$this->reset();
		return $item_id;
	}

	public function update($data, $search) {

		$where_arr = $where_in_arr = [];

		foreach ($search as $key => $val) {
			switch ($key) {
				case '_field_like':
					$where_arr[] = [
						'key' => $this->table . '.' . $val['field'] . ' LIKE "%' . $val['value'] . '%"'
					];
					break;
				case '_in':
					$where_in_arr[] = [
						'key' => $this->table . '.' . $val['field'],
						'values' => $val['value']
					];
					break;
				default :
					$where_arr[] = [
						'key' => $this->table . '.' . $key,
						'value' => $val
					];

					break;
			}
		}

		$this->setWhere($where_arr);
		$this->setWhere_in($where_in_arr);

		$this->db->update($this->table, $data);
		if ($this->db->_error_number()) {
			$this->reset();
			throw new SimpleDbModelException('Ошибка записи в базу: ' . $this->db->_error_message());
		}
		$this->reset();
		return true;
	}

	public function get($params = []) {
		$params['limit']['from'] = 0;
		$params['limit']['count'] = 1;
		$items = $this->getAll($params);
		if (!empty($items)) {
			return $items[0];
		}
		return $items;
	}

	public function changeAmount($field, $change_amount, $search = [], $increase = true) {

		$field = $this->table . '.' . $field;
		$where_arr = [];
		foreach ($search as $key => $val) {
			switch ($key) {
				default :
					$where_arr[] = [
						'key' => $this->table . '.' . $key,
						'value' => $val
					];
					break;
			}
		}

		$this->setWhere($where_arr);

		$this->db->set($field, $field . ($increase ? '+' : '-') . $change_amount, false);
		$this->db->update($this->table);

		if ($this->db->_error_number()) {
			$this->reset();
			throw new SimpleDbModelException('Ошибка записи в базу: ' . $this->db->_error_message());
		}
		$this->reset();
		return $this->db->affected_rows();
	}

	public function deleteAll($search = [], $forse_delete_without_condition = false) {
		if (empty($search) && !$forse_delete_without_condition) {
			throw new SimpleDbModelException('Удаление без условий запрещено');
		}

		$where_arr = $where_in_arr = [];

		foreach ($search as $key => $val) {
			switch ($key) {
				case '_in':
					$where_in_arr[] = [
						'key' => $this->table . '.' . $val['field'],
						'values' => $val['value']
					];
					break;
				default :
					$where_arr[] = [
						'key' => $this->table . '.' . $key,
						'value' => $val
					];
					break;
			}
		}

		$this->setWhere($where_arr);
		$this->setWhere_in($where_in_arr);
		$this->db->delete($this->table);

		if ($this->db->_error_number()) {
			$this->reset();
			throw new SimpleDbModelException('Ошибка записи в базу: ' . $this->db->_error_message());
		}
		$this->reset();
		return true;
	}

	public function setTable($val) {
		$this->table = $val;
	}

	protected function setJoin() {
		usort($this->join_arr, function ($a, $b) {

			$sort_key = 'sorter';

			if (!isset($a[$sort_key])) {
				return 1;
			}
			if (!isset($b[$sort_key])) {
				return -1;
			}
			$a_val = (int) $a[$sort_key];
			$b_val = (int) $b[$sort_key];

			if ($a_val > $b_val)
				return 1;
			if ($a_val < $b_val)
				return -1;

			return 0;
		});
		foreach ($this->join_arr as $val) {
			$this->db->join($val['table'], $val['on'], isset($val['param']) ? $val['param'] : '');
		}
	}

	protected function setWhere($where_arr) {
		foreach ($where_arr as $val) {
			if (!isset($val['value'])) {
				$val['value'] = null;
			}
			if (!isset($val['escape'])) {

				$val['escape'] = true;
			}
			$this->db->where($val['key'], $val['value'], $val['escape']);
		}
	}

	protected function setWhere_in($where_in_arr) {
		foreach ($where_in_arr as $val) {
			if (!isset($val['values'])) {
				$val['values'] = null;
			}
			$this->db->where_in($val['key'], $val['values']);
		}
	}

	protected function setFields($fields) {
		foreach ($fields as $key => $field) {
			switch ($field) {
				default : $this->select_arr[$field] = $this->table . '.' . $field;
			}
		}
	}

	protected function setSearch($search) {
		foreach ($search as $key => $val) {
			switch ($key) {
				case '_field_like':
					$this->where_arr[] = [
						'key' => $this->table . '.' . $val['field'] . ' LIKE "%' . $val['value'] . '%"'
					];
					break;
				case '_in':
					$this->where_in_arr[] = [
						'key' => $this->table . '.' . $val['field'],
						'values' => $val['value']
					];
					break;
				default :
					$this->where_arr[] = [
						'key' => $this->table . '.' . $key,
						'value' => $val
					];

					break;
			}
		}
	}

	protected function additionalLoop() {
		//if(!$this->need_additional_loop){return;}
//		$fields = !empty($this->params['fields']) ? $this->params['fields'] : [];
//		foreach ($this->items as &$item) {
//			
//		}
	}

}

class SimpleDbModelException extends MyException {
	
}
