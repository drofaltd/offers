<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Token_model extends Simple_db_model {

	public function __construct() {
		parent::__construct();
		parent::setTable('token');
	}

	public function createToken($user_id, $type) {
		$this->deleteAll(array('user_id' => $user_id, 'type' => $type));
		$token = $this->generateToken();
		$data = array(
			'user_id' => $user_id,
			'type' => $type,
			'token' => $token,
			'date_add' => date('Y-m-d H:i:s')
		);
		$this->save($data);
		
		return $token;
	}

	public function checkToken($user_id, $type, $token) {
		return $this->getCount(array('user_id' => $user_id, 'type' => $type, 'token' => $token));
	}

	private function generateToken() {
		$token = md5(microtime() . rand(0, 1000));
		$exists_token = $this->get(array('search' => array('token' => $token)));
		if (!$exists_token) {
			return $token;
		} else {
			$this->generateToken();
		}
	}

}
