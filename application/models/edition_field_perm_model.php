<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Edition_field_perm_model extends Simple_db_model {

	public $can_view_perm_type = 'can_view';
	public $can_edit_perm_type = 'can_edit';

	public function __construct() {
		parent::__construct();
		parent::setTable('edition_field_perm');
	}

	public function getRoleIdsByType($type, $field_id) {
		$items = $this->getAll(['fields' => ['role_id'], 'search' => ['field_id' => $field_id, 'type' => $type]]);
		if (empty($items)) {
			return [];
		}
		return group_db_field('role_id', $items);
	}

	public function saveFieldPerms($type, $role_ids, $field_id) {
		$this->deleteAll(['type' => $type, 'field_id' => $field_id]);
		if(empty($role_ids)){
			return;
		}
		foreach ($role_ids as $role_id) {
			$this->save(['type' => $type, 'field_id' => $field_id, 'role_id' => $role_id]);
		}
	}

}

class EditionFieldPermModelException extends SimpleDbModelException {
	
}
