<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Offer_model extends Simple_db_model {

	public $offer_item_fields = ['username', 'edition_ipn', 'edition_rin', 'edition_fields'];

	public function __construct() {
		parent::__construct();
		parent::setTable('offer');
		$this->load->model('edition_field_model');
	}

	protected function setFields($fields) {
		foreach ($fields as $key => $field) {
			switch ($field) {
				case 'edition_ipn':
					$this->setSearch(['last_edition' => 1]);
					$this->select_arr['edition_ipn'] = 'last_edition.ipn AS edition_ipn';
					break;
				case 'edition_rin':
					$this->setSearch(['last_edition' => 1]);
					$this->select_arr['edition_rin'] = 'last_edition.rin AS edition_rin';
					break;
				case 'edition_fields':
					foreach ($this->params['param']['ed_fields'] as $ed_field) {
						$alias = 'edition_vield_value_' . $ed_field['id'];

						$this->select_arr[$this->edition_field_model->edition_field_prefix . $ed_field['id']] = $alias . '.value AS ' . $this->edition_field_model->edition_field_prefix . $ed_field['id'];

						$this->join_arr[$alias] = [
							'table' => 'edition_field_value ' . $alias,
							'on' => 'last_edition.id=' . $alias . '.edition_id AND ' . $alias . '.field_id = ' . $ed_field['id'],
							'param' => 'left'
						];
					}

					break;
				case 'username':
					$alias = 'user';
					$this->join_arr[$alias] = [
						'table' => 'user ' . $alias,
						'on' => $this->table . '.user_id=' . $alias . '.id',
						'sorter' => 0
					];
					$this->select_arr[$field] = 'CONCAT(' . $alias . '.firstname," ",' . $alias . '.lastname) AS ' . $field;
					break;
				default : $this->select_arr[$field] = $this->table . '.' . $field;
			}
		}
	}

	protected function setSearch($search) {
		foreach ($search as $key => $val) {
			switch ($key) {
				case 'last_edition':
					$alias = 'last_edition';
					$this->join_arr[$alias] = [
						'table' => 'edition ' . $alias,
						'on' => $this->table . '.id = ' . $alias . '.offer_id',
						'sorter' => 1
					];

					$this->where_arr[] = [
						'key' => $alias . '.id = ( SELECT id FROM edition WHERE offer_id = ' . $this->table . '.id ORDER BY id DESC LIMIT 1 )'
					];

					$this->db->_protect_identifiers = false;
					break;
				case '_field_like':
					$this->where_arr[] = [
						'key' => $this->table . '.' . $val['field'] . ' LIKE "%' . $val['value'] . '%"'
					];
					break;
				case '_in':
					$this->where_in_arr[] = [
						'key' => $this->table . '.' . $val['field'],
						'values' => $val['value']
					];
					break;
				default :
					$this->where_arr[] = [
						'key' => $this->table . '.' . $key,
						'value' => $val
					];

					break;
			}
		}
	}

}

class OfferModelException extends SimpleDbModelException {
	
}
