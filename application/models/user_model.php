<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class User_model extends Simple_db_model {

	public function __construct() {
		parent::__construct();
		parent::setTable('user');
	}

	public function getAll($params = array()) {
		$fields = isset($params['fields']) ? $params['fields'] : array();
		$search = isset($params['search']) ? $params['search'] : array();
		$from = isset($params['limit']['from']) ? $params['limit']['from'] : 0;
		$count = isset($params['limit']['count']) ? $params['limit']['count'] : 0;
		$order = isset($params['order']) ? $params['order'] : array();

		$additional_loop = $filtered_select = false;

		if ($from || $count) {
			$this->db->limit($count, $from);
		}

		$select_arr = [$this->table . '.id'];

		$join_arr = $group_by_arr = array();


		if (!empty($order)) {
			foreach ($order as $key => $val) {
				switch ($key) {
					default :$this->db->order_by($this->table . '.' . $key, $val);
				}
			}
		}

		foreach ($fields as $field) {
			switch ($field) {
				case 'role_name':
					$alias = 'user_role';
					$join_arr[] = [
						'table' => 'user_role ' . $alias,
						'on' => $this->table . '.id=' . $alias . '.user_id'
					];
					$alias2 = 'role';
					$join_arr[] = [
						'table' => 'role ' . $alias2,
						'on' => $alias . '.role_id=' . $alias2 . '.id'
					];

					$select_arr[$field] = $alias2 . '.name AS ' . $field;
					break;
				default : $select_arr[$field] = $this->table . '.' . $field;
			}
		}

		$select_str = implode(',', $select_arr);
		$this->db->select($select_str, $filtered_select);
		$this->db->from($this->table);

		foreach ($search as $key => $val) {
			switch ($key) {
				case '_in':
					$this->db->where_in($this->table . '.' . $val['field'], $val['value']);
					break;
				default :
					$this->db->where($this->table . '.' . $key, $val);
					break;
			}
		}

		if (!empty($join_arr)) {
			foreach ($join_arr as $val) {
				$this->db->join($val['table'], $val['on'], isset($val['param']) ? $val['param'] : '');
			}
		}


		if (!empty($group_by_arr)) {
			foreach ($group_by_arr as $val) {
				$this->db->group_by($val);
			}
		}

		$items = $this->db->get()->result('array');

		if ($additional_loop) {
			foreach ($items as &$item) {
				
			}
		}
		return $items;
	}

	public function getCount($search = []) {
		$this->db->from($this->table);
		foreach ($search as $key => $val) {
			switch ($key) {
				case '_in':
					$this->db->where_in($this->table . '.' . $val['field'], $val['value']);
					break;
				default :
					$this->db->where($this->table . '.' . $key, $val);
					break;
			}
		}

		if (!empty($join_arr)) {
			foreach ($join_arr as $val) {
				$this->db->join($val['table'], $val['on'], isset($val['param']) ? $val['param'] : '');
			}
		}

		if (!empty($group_by_arr)) {
			foreach ($group_by_arr as $val) {
				$this->db->group_by($val);
			}
		}

		$res = $this->db->count_all_results();
		return $res;
	}

	public function registerUser($user_data) {
		$user_id = $this->saveUser($user_data);
		if (!$user_id) {
			return false;
		}
		//generate token and send activation email
		if (!empty($user_data['user']['email']) && !$this->sendActivationEmail($user_id, $user_data['user']['email'], $user_data['user']['password'])) {
			return false;
		}

		return true;
	}

	public function login($email, $password) {
		$this->access->loginUser($email, $password);
	}

	public function rememberMe($email, $password) {
		$this->access->forgotUser();
		$this->access->rememberUser($email, $password);
		return true;
	}

	public function forgotPassword() {
		$this->load->library('form_validation');
		if ($this->form_validation->run()) {
			$user = $this->get(array('fields' => array('email'), 'search' => array('email' => $this->input->post('email', TRUE))));
			if (!$user) {
				throw new UserModelException('Данный email в базе не найден.');
			}
			if (!$this->sendForgotPasswordEmail($user['id'], $user['email'])) {
				return false;
			}
			return true;
		}
	}

	public function checkResetPasswordToken() {
		$this->load->model('token_model');

		$user_id = $this->input->get('user_id', TRUE);
		$token = $this->input->get('token', TRUE);

		return $this->token_model->checkToken($user_id, 'forgot_password', $token);
	}

	public function resetPassword($data, $user_id) {
		return $this->save($data, $user_id);
	}

	public function saveUser($user_data, $user_id = 0) {
		$scenario = $user_id ? 'edit' : 'add';
		if (!empty($user_data['user'])) {
			if (!empty($user_data['user']['password'])) {
				$user_data['user']['password'] = $this->encodePassword($user_data['user']['password']);
			}
			if ($scenario == 'add') {
				$user_data['user']['date_add'] = date('Y-m-d H:i:s');
			}
			$user_id = $this->save($user_data['user'], $user_id);
		} elseif (!$user_id) {
			throw new UserModelException('Для нового пользователя не указаны первичные данные');
		}

		if ($scenario == 'add') {
			$this->load->model('user_balance_model');
			$this->user_balance_model->save(['user_id' => $user_id, 'date_modify' => date('Y-m-d H:i:s')]);
			$this->subscribeUserToAllSubscriptions($user_id);
		}

		return $user_id;
	}

	private function sendActivationEmail($user_id, $email, $password = '') {
		$this->load->model('token_model');

		$token = $this->token_model->createToken($user_id, 'activate');

		$data['scenario'] = $password ? 'register' : 'activation_request';

		$user = $this->user_model->get(['fields' => ['profile_name'], 'search' => ['id' => $user_id]]);

		$data['activation_url'] = base_url('/site/activate/user_id/' . $user_id . '/token/' . $token);
		$data['username'] = $user['profile_name'];
		$data['email'] = $email;
		$data['password'] = $password;
		$this->layout->setLayout('layout/email');
		$msg = $this->layout->view('emails/activation', $data, true);
		$this->layout->setLayout('layout/main');

		$this->load->library('email');

		$this->email->from($this->config->item('site_email'));
		$this->email->to($email);
		$this->email->subject($data['username'] . ', пожалуйста, подтвердите регистрацию на Profinar.ru');
		$this->email->message($msg);

		if (!$this->email->send()) {
			throw new UserModelException('Ошибка отправки почты, обратитесь к администратору сайта');
		}
		//echo $this->email->print_debugger();exit;
		return $res;
	}

	private function sendForgotPasswordEmail($user_id, $email) {
		$this->load->model('token_model');
		$token = $this->token_model->createToken($user_id, 'forgot_password');

		$forgot_password_url = base_url('/site/reset_password/user_id/' . $user_id . '/token/' . $token);
		$this->layout->setLayout('layout/email');
		$msg = $this->layout->view('emails/forgot_password', array('forgot_password_url' => $forgot_password_url), true);
		$this->layout->setLayout('layout/main');

		$this->load->library('email');

		$this->email->from($this->config->item('site_email'));
		$this->email->to($email);
		$this->email->subject('Восстановление пароля на сайте ' . base_url());
		$this->email->message($msg);
		$res = $this->email->send();
		if (!$res) {
			throw new UserModelException('Ошибка отправки почты, обратитесь к администратору сайта');
		}
//		echo $this->email->print_debugger();
//		exit;
		return $res;
	}

	public function encodePassword($val) {
		return md5($val);
	}

	public function getUserLink($user_id) {
		return base_url('/user/view/id/' . $user_id);
	}

	public function deleteUsers($ids) {
		foreach ($ids as $id) {
			if (!$this->deleteUser($id)) {
				return false;
			}
		}
		return true;
	}

	public function deleteUser($id) {
		$user = $this->get(['fields' => ['email'], 'search' => ['id' => $id]]);
		if (empty($user)) {
			throw new UserModelException('Пользователь с id "' . $id . '" не найден');
		}

		$this->load->model('school_model');
		$this->load->model('webinar_model');
		$this->load->library('pimages');

		$school_id = $this->userHaveSchool($id);

		if ($school_id) {
			$this->school_model->deleteSchool($school_id);
		}

		$this->pimages->deleteUpload('profile', ['replace' => ['[user_id]' => $id]]);

		$this->sendDeletedEmail($user['email']);
		$this->deleteAll(['id' => $id]);

		return true;
	}

	public function setSessionTimeZone($timezone) {
		$user = $this->session->userdata('user');
		$user['timezone'] = $timezone;
		$this->session->set_userdata('user', $user);
	}

}

class UserModelException extends SimpleDbModelException {
	
}
