<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
class Edition_field_value_model extends Simple_db_model {
	 
	public function __construct() {
		parent::__construct();
		parent::setTable('edition_field_value');
	}
	
}

class EditionFieldValueModelException extends SimpleDbModelException {
	
}
