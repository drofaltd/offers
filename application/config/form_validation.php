<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

$config['site/login'] = [
	[
		'field' => 'email',
		'label' => 'Email',
		'rules' => 'required|xss_clean|valid_email'
	],
	[
		'field' => 'password',
		'label' => 'Пароль',
		'rules' => 'required|xss_clean|min_length[6]'
		],
];


$config['offersTableEditorAjax/saveField'] = [
	[
		'field' => 'type',
		'label' => 'Тип поля',
		'rules' => 'required|xss_clean'
		],
	[
		'field' => 'can_edit_role_ids',
		'label' => 'Разрешено редактировать',
		'rules' => 'required|xss_clean'
		],
];

$config['offersTableEditorAjax/saveSorters'] = [
	[
		'field' => 'sorters',
		'label' => 'сортеры',
		'rules' => 'required|xss_clean'
	],
	
];