<?php

/**
 * @author Pavel Ivanov <pavelivanovv@gmail.com>
 */
$config['admin'] = ['allow' => ['*']];

$config['manager'] = [
	'allow' => [
		'Site::index'
	]
];
