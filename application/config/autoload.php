<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$autoload['packages'] = [];

$autoload['libraries'] = ['session','MyException','Layout','Access'];

$autoload['helper'] = ['url','cookie','array'];

$autoload['config'] = ['sec_config'];

$autoload['language'] = [];


$autoload['model'] = ['simple_db_model','user_model'];


/* End of file autoload.php */
/* Location: ./application/config/autoload.php */