var wrong_request_error = 'Ошибка запроса, перезагрузите страницу и попробуйте снова';
setTimeout(function() {
	initScroll()
}, 1000);
function initScroll() {
	if (sessionStorage.scrollX != undefined && sessionStorage.scrollY != undefined) {
		delete sessionStorage.scrollX;
		delete sessionStorage.scrollY;
	}
}

function showNotice(msg) {
	alert(msg);
}

function encodeURIJSON(json) {
	return encodeURIComponent(JSON.stringify(json));
}

function modal(content) {
	modal(content, {});
}
function modal(content, params) {
	if (!params) {
		params = {};
	}
	if (params.width == undefined) {
		params.width = 850;
	}
	if (params.height == undefined) {
		params.height = 'auto';
	}
	if (params.dialogClass == undefined) {
		params.dialogClass = 'modal-wr';
	}
	if (params.draggable == undefined) {
		params.draggable = false;
	}
	if (params.modal == undefined) {
		params.modal = true;
	}
	if (params.title == undefined) {
		params.title = '';
	}

	$('#modal-box').html(content).dialog(params);
	return $('#modal-box');
}

function modalClose() {
	$('#modal-box').dialog('close');
}

function expldeObjectIdRev(objectIdRev) {
	return objectIdRev.split('-');
}

function saveScrollToSessionStorage() {
	sessionStorage.scrollX = window.scrollX;
	sessionStorage.scrollY = window.scrollY;
}

function sessionStorageScrollTo() {
	if (sessionStorage.scrollX != undefined && sessionStorage.scrollY != undefined) {
		window.scrollTo(sessionStorage.scrollX, sessionStorage.scrollY);
		sessionStorage.scrollX = undefined;
		sessionStorage.scrollY = undefined
	}
}


function parseHex(hexStr) {
	if (hexStr.charAt(0) == '#') {
		hexStr = hexStr.substr(1);
	}
	var res = (parseInt("0x" + hexStr, 16)).toString(16);
	if (res !== 'NaN') {
		return '#' + res;
	} else {
		return res
	}
}

var arrayUnique = function(a) {
	return a.reduce(function(p, c) {
		if (p.indexOf(c) < 0)
			p.push(c);
		return p;
	}, []);
};

function randomStr(len){
	if(len == undefined){
		len = 8;
	}

    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < len; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}