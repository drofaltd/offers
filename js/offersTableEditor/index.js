$(function() {
	var $currEditFieldItem = null;

	$(".sortable").sortable({
		update: function(event, ui) {
			updateSorters();
		}
	});
	$(".sortable").disableSelection();

	$(document).on('click', '.add-field-button', function() {
		editFieldForm();
	});
	$(document).on('click', '.edit-field-button', function(e) {
		editFieldForm($(e.target).parents('.field-item:first'));
	});
	$(document).on('click', '.add-field-type-list-option-button', function(e) {
		addTypeOptionItem($('.edit-field-form.showing .field-type-list-options'), {name: '', value: genOptionValueValue()})
	});
	$(document).on('click', '.delete-field-type-list-option-button', function(e) {
		deleteTypeOptionItem($(e.target).parents('.field-type-list-option-item:first'));
	});
	$(document).on('change', '.edit-field-form select[name=type]', function(e) {
		initTypeOptionsWrapper();
	});
	$(document).on('submit', '.edit-field-form', function(e) {
		e.preventDefault();
		saveFieldForm(e);
	});

	initFieldsTable();

	function initFieldsTable() {
		var i = 0, fieldsLength = fileds.length;
		for (i; i < fieldsLength; i++) {
			addFieldItem(fileds[i]);
		}
	}

	function initColorPickers($form) {
		$form.find('.bgColorSelector div').css('backgroundColor', '#' + $form.find('input[name=bg_color]').val());

		$form.find('.bgColorSelector').ColorPicker({
			color: '#' + $form.find('input[name=bg_color]').val(),
			onShow: function(colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function(colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function(hsb, hex, rgb) {
				$form.find('input[name=bg_color]').val(hex);
				$form.find('.bgColorSelector div').css('backgroundColor', '#' + hex);
			}
		});

		$form.find('.colorSelector div').css('backgroundColor', '#' + $form.find('input[name=color]').val());
		$form.find('.colorSelector').ColorPicker({
			color: '#' + $form.find('input[name=color]').val(),
			onShow: function(colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function(colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function(hsb, hex, rgb) {
				$form.find('input[name=color]').val(hex);
				$form.find('.colorSelector div').css('backgroundColor', '#' + hex);
			}
		});
	}

	function updateFieldItem($fieldItem, fieldInfo) {
		var $newfieldItem = genFieldItem(fieldInfo);
		$fieldItem.replaceWith($newfieldItem);
	}

	function addFieldItem(fieldInfo) {
		var $fieldItem = genFieldItem(fieldInfo);
		$('.fields-edit-list').append($fieldItem);
	}

	function genFieldItem(fieldInfo) {
		var $tpl = $('.field-item-tpl').clone(true);
		var $fieldItem = $($tpl.html());
		$fieldItem.attr('data-id', fieldInfo.id);

		$fieldItem.find('.name-wr').html(fieldInfo.name);

		var color = parseHex(fieldInfo.color);
		if (color !== 'NaN') {
			$fieldItem.css('color', color);
		}
		var bg_color = parseHex(fieldInfo.bg_color);
		if (bg_color !== 'NaN') {
			$fieldItem.css('background-color', bg_color);
		}
		return $fieldItem;
	}

	function genFieldItemTtitleAttr(fieldInfo) {

	}

	function editFieldForm($fieldItem) {
		var $form = $($('.edit-field-form-tpl').clone(true).html());

		$form.find('.edit-field-form').addClass('showing');

		if ($fieldItem == undefined) {
			$form.find('input[name=bg_color]').val('fff');
			$form.find('input[name=color]').val('000');
			modal($form);

			initColorPickers($form);
		} else {
			getFormFieldInfo($fieldItem.attr('data-id'), function(data) {
				$currEditFieldItem = $fieldItem;
				formFields = data.form_fields;
				$form = fillEditFieldForm($form, data.form_fields);
				modal($form);

				initTypeOptionsWrapper();

				initColorPickers($form);
				return;
			});
		}


	}

	function getFormFieldInfo(fieldId, cb) {
		$.post(BASE_URL + 'offersTableEditorAjax/getFormFieldInfo', {field_id: fieldId}, function(data) {
			if (data == null || data.state == undefined) {
				showNotice(wrong_request_error);
				return;
			}
			switch (data.state) {
				case'redirect':
					location.href = data.url;
					break;
				case 'ok':
					cb(data);
					break;
				case 'error':
					showNotice(data.msg);
					break;
				default:
					showNotice(wrong_request_error);
					return;
			}
		})
	}

	function fillEditFieldForm($form, formFields) {
		$form.find('input[name=field_id]').val(formFields.id);
		$form.find('input[name=bg_color]').val(formFields.bg_color);
		$form.find('input[name=color]').val(formFields.color);
		$form.find('input[name=name]').val(formFields.name);
		$form.find('select[name=type]').val(formFields.type);
		$form.find('input[name="can_view_role_ids[]"]').prop('checked', false);

		var i = 0, canViewPerms = formFields.can_view_perms, canViewPermsLength = formFields.can_view_perms.length;

		for (i; i < canViewPermsLength; i++) {
			$form.find('input[name="can_view_role_ids[]"][value="' + canViewPerms[i] + '"]').prop('checked', true);
		}

		$form.find('input[name="can_edit_role_ids[]"]').prop('checked', false);
		var i = 0, canEditPerms = formFields.can_edit_perms, canEditPermsLength = formFields.can_edit_perms.length;

		for (i; i < canEditPermsLength; i++) {
			$form.find('input[name="can_edit_role_ids[]"][value="' + canEditPerms[i] + '"]').prop('checked', true);
		}

		return $form;
	}

	function saveFieldForm(e) {
		var $form = $(e.target);

		if (!validateFieldTypeOptsForm($form)) {
			return;
		}

		$.post(BASE_URL + 'offersTableEditorAjax/saveField', $form.serialize(), function(data) {
			if (data == null || data.state == undefined) {
				showNotice(wrong_request_error);
				return;
			}
			switch (data.state) {
				case'redirect':
					location.href = data.url;
					break;
				case 'ok':
					modalClose();
					if ($form.find('input[name=field_id]').val() == 0) {
						addFieldItem(data.field_info);
						updateSorters();
					} else {
						updateFieldItem($currEditFieldItem, data.field_info);
					}

					break;
				case 'error':
					showNotice(data.msg);
					break;
				default:
					showNotice(wrong_request_error);
					return;
			}
		})
	}

	function validateFieldTypeOptsForm($form) {
		if ($.inArray($form.find('select[name=type]').val(), listInputs) === -1) {
			return true;
		}

		var $nameOpts = $form.find('input[name="field_type_option_name[]"]');
		var $valueOpts = $form.find('input[name="field_type_option_value[]"]');

		//нарушена структура формы
		if ($nameOpts.length == 0 || $valueOpts.length == 0 || $nameOpts.length != $valueOpts.length) {
			showNotice('Ошибка формы, перезагрузите страницу и попробуйте снова');
			return false;
		}

		var nameVals = [], valueVals = [];
		$nameOpts.each(function() {
			if ($(this).val() != '') {
				nameVals.push($(this).val())
			}
		});
		$valueOpts.each(function() {
			if ($(this).val() != '') {
				valueVals.push($(this).val())
			}
		});

		if (nameVals.length != valueVals.length) {
			showNotice('Каждому наименованию опции должно быть присвоено значение и наоборот.');
			return false;
		}

		if (nameVals.length == 0) {
			showNotice('Заполните хотябы одну пару полей для опции');
			return false;
		}

		if (nameVals.toString() != arrayUnique(nameVals).toString()) {
			showNotice('Значения имен опций должны быть уникальными');
			return false;
		}

		if (valueVals.toString() != arrayUnique(valueVals).toString()) {
			showNotice('Значения значений опций должны быть уникальными');
			return false;
		}

		return true;
	}

	function updateSorters() {
		var sorters = getSorters();

		$.post(BASE_URL + 'offersTableEditorAjax/saveSorters', {sorters: sorters}, function(data) {
			if (data == null || data.state == undefined) {
				showNotice(wrong_request_error);
				return;
			}
			switch (data.state) {
				case'redirect':
					location.href = data.url;
					break;
				case 'ok':
					break;
				case 'error':
					showNotice(data.msg);
					break;
				default:
					showNotice(wrong_request_error);
					return;
			}
		})
	}

	function getSorters() {
		var sorters = {};
		$('.fields-edit-list .field-item').each(function(i) {
			sorters[$(this).attr('data-id')] = i;
		})
		return sorters;
	}

	function initTypeOptionsWrapper() {
		var type = $('.edit-field-form.showing select[name=type]').val();

		if ($.inArray(type, listInputs) !== -1) {
			var $optCont = $($('.list-options-wr-tpl').clone(true).html());
			var $optWr = $optCont.find('.field-type-list-options');
			if (formFields.type_options == undefined) {
				formFields.type_options = [];
			}
			if (formFields.type_options.list == undefined) {
				formFields.type_options.list = [];
			}

			if (formFields.type_options.list.length == 0) {
				addTypeOptionItem($optWr);
			} else {
				fillWithTypeOptionItems($optWr, formFields.type_options.list);
			}
			$('.edit-field-form.showing .field-type-options-wr').html($optCont);

		} else {

			$('.edit-field-form.showing .field-type-options-wr').html('');
		}
		$('.field-type-list-options-sortable').sortable();
	}

	function addTypeOptionItem($optsWr, optParams) {
		var $item = $($('.field-type-list-option-item-tpl').clone(true).html());
		if (optParams != undefined) {
			if (optParams.name != undefined) {
				$item.find('input[name="field_type_option_name[]"]').val(optParams.name);
			}
			if (optParams.value != undefined) {
				$item.find('input[name="field_type_option_value[]"]').val(optParams.value);
			}
		}
		$optsWr.append($item);
		if ($optsWr.find('.field-type-list-option-item').length > 1) {
			$optsWr.find('.delete-field-type-list-option-button').removeClass("hide")
		} else {
			$optsWr.find('.delete-field-type-list-option-button').addClass("hide")
		}

	}

	function fillWithTypeOptionItems($optWr, items) {
		var i = 0, itemsLength = items.length;
		for (i; i < itemsLength; i++) {
			addTypeOptionItem($optWr, items[i]);
		}
	}

	function deleteTypeOptionItem($item) {
		var $parentItem = $item.parents('.field-type-list-options'), itemsLength = $parentItem.children('.field-type-list-option-item').length;

		if (itemsLength > 1) {
			$item.remove();
		}
		console.log($parentItem);
		console.log(itemsLength);
		if (itemsLength < 3) {
			$parentItem.find('.delete-field-type-list-option-button').addClass("hide");
		}
	}

	function genOptionValueValue() {
		return randomStr();
	}
	
	
})