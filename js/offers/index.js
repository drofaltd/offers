$(function() {

	$(document).on('click', '.edit-offer-button', function(e) {
	});
	$(".offers-table .resizable").resizable({
		stop: function( event, ui ) {
			setTableElemsSizes(ui.element,ui.size.width,ui.size.height);			
		}
	});
	
	function setTableElemsSizes($elem,width,height){
		var elemIndex = $elem.index();
		$('.offer-item').each(function(){
			var $el = $($(this).find('.resizable')[elemIndex]);
			$($(this).find('td .resizable')[elemIndex]).css({width:width});
		});
		
		$elem.parents('.offer-item').find('.resizable').css({height:height});
	}
})